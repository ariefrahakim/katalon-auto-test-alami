package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p>Profile default : tokenLogin</p>
     */
    public static Object access_token
     
    /**
     * <p></p>
     */
    public static Object url
     
    /**
     * <p></p>
     */
    public static Object id_rolepermission
     
    /**
     * <p></p>
     */
    public static Object id_admin
     
    /**
     * <p></p>
     */
    public static Object id_activity
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += TestCaseMain.getParsedValues(RunConfiguration.getOverridingParameters())
    
            access_token = selectedVariables['access_token']
            url = selectedVariables['url']
            id_rolepermission = selectedVariables['id_rolepermission']
            id_admin = selectedVariables['id_admin']
            id_activity = selectedVariables['id_activity']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
