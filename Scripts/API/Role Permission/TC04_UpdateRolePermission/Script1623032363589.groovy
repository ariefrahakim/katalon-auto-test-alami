import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import groovy.json.JsonSlurper as JsonSlurper

'User send requst put role and permission'
def putRequest = WS.sendRequest(findTestObject('API/Role Permission/CIAM - UpdateRolePermission', [('ipCiam') : GlobalVariable.url
            , ('auth_token') : GlobalVariable.access_token, ('id') : GlobalVariable.id_rolepermission]))

'Verify status code'
def verifiedCode = WS.verifyResponseStatusCode(putRequest, 200)

'Parsing response body content'
JsonSlurper parsing = new JsonSlurper()
def afterParsing = parsing.parseText(putRequest.getResponseBodyContent())

'Success update data message'
def successMessageUpdate = afterParsing.statusDescription
println(successMessageUpdate)