<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_RegressionTestAPI</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ff9638e5-1c75-4f15-b155-8b9985334050</testSuiteGuid>
   <testCaseLink>
      <guid>e2afb1b8-7237-4526-8236-cb3b39b7a687</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Admin/TC01_CreateAdmin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57a7724f-41bd-4a74-89cf-579b4cdc6e21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Admin/TC02_GetAdmin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1bca70c7-d2cf-41a0-b633-fb84759de875</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Admin/TC03_GetDetailsAdmin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d18cbcc5-109f-44ee-8a7e-dbbfde27bf84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Admin/TC04_UpdateAdmin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73f5d3f9-dcd1-49eb-9bf1-4d8dd0676401</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Admin/TC05_DeleteAdmin</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
