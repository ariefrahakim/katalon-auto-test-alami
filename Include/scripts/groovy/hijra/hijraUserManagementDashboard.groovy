package hijra

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


import java.text.SimpleDateFormat
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class hijraUserManagementDashboard {

	//	def date = new Date()
	//	def sdf = new SimpleDateFormat('HH-mm-ss.SSS')
	//	String today = sdf.format(date)

	@Given("user klik menu user management dashboard")
	def user_klik_menu_user_management_dashboard() {
		WebUI.click(findTestObject('Object Repository/User Management Dashboard/menuUserManagementDashboard'))
		WebUI.delay(1)
	}

	@When("user klik add new user")
	def user_klik_add_new_user() {
		WebUI.click(findTestObject('Object Repository/User Management Dashboard/addNewUser'))
		WebUI.delay(1)
	}

	@Then("verify landing page add new user")
	def verify_landing_page_add_new_user() {
		WebUI.verifyElementVisible(findTestObject('Object Repository/User Management Dashboard/verifyLandingAddNewUser'))
		WebUI.delay(1)
	}

	@And("user input name")
	def user_input_name() {
		WebUI.setText(findTestObject('Object Repository/User Management Dashboard/inputName'), 'TestAja')
		WebUI.delay(1)
	}

	@And("user input email")
	def user_input_email() {
		WebUI.setText(findTestObject('Object Repository/User Management Dashboard/inputEmail'), 'test123@gmail.com')
		WebUI.delay(1)
	}

	@And("user input phone")
	def user_input_phone() {
		WebUI.setText(findTestObject('Object Repository/User Management Dashboard/inputPhone'), '08213182319')
		WebUI.delay(1)
	}
	
	@And("user select role")
	def user_select_role() {
		WebUI.selectOptionByValue(findTestObject('Object Repository/User Management Dashboard/selectRole'), 'DIRECTOR_ADMIN', false)
		WebUI.delay(1)
	}

	@And("user select domain")
	def user_select_domain() {
		WebUI.selectOptionByValue(findTestObject('Object Repository/User Management Dashboard/selectDomain'), 'LEGAL', false)
		WebUI.delay(1)
	}
	
	@And("user klik save")
	def user_klik_save() {
		WebUI.click(findTestObject('Object Repository/User Management Dashboard/buttonSave'))
		WebUI.delay(1)
		WebUI.closeBrowser()
	}

	//	@And("verify success add user")
	//	def verify_success_add_user() {
	//		WebUI.verifyElementVisible(findTestObject('Object Repository/User Management Dashboard/verifySuksesMenambahkanUser'))
	//		WebUI.delay(3)

	//	}

}